Services raw provides access to resources of services module via direct usage
of API calls like node_save or user_save instead of executing form programatically.

Supported resources:
* Node - CREATE, UPDATE - node_save
* User - CREATE, UPDATE - user_save

To enhance security module also introduces new permissions like 'access raw node_save'.
This means that user logged in via services must have these special permissions to 
use services_raw resources.

TODO:
* Add support for comment, taxonomy, files
